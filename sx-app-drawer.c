// sx-app-drawer.c - Scan for .desktop files and display them

#include <stdio.h>    // the basics
#include <stdlib.h>   // provides free()
#include <dirent.h>   // for parsing directories
#include <string.h>   // strings
#include "termbox.h"  // TUI

// potential locations for .desktop files
char * system_apps       = "/usr/share/applications/";
char * local_system_apps = "/usr/local/share/applications/";
char * local_apps_suffix = "/.local/share/applications/";  // gets prefixed with home dir later

// Functions-------------------------------------------------------------------

// check if item is a .desktop file
int is_desktop_file(char * item)
{
	char * suffix   = ".desktop";
	char * is_found = strstr(item, suffix);

	if (is_found == NULL)
	{
		return 0;  // false
	}

	return 1;  // true
}

// read directory dir_name and print all .desktop files in it
void read_dir(char * dir_name)
{
	// setup variables
	DIR * directory;
	struct dirent ** name_list;
	int file_count = 0;

	directory = opendir(dir_name);  // open directory

	// exit function, if directory does not exist
	if (directory == NULL)
	{
		printf("  Unable to read directory: %s, it probably doesn't exist.\n", dir_name);
		return;
	}

	printf("  Directory is opened: %s\n", dir_name);

	file_count = scandir(dir_name, &name_list, NULL, alphasort);  // get number of files and populate name_list with items in directory

	// loop through name_list and print each file name
	int i = 0;
	while (i < file_count)
	{
		char * filename = name_list[i]->d_name;

		if (is_desktop_file(filename))
		{
			printf("    File %3d: %s\n", i + 1, filename);
		}

		free(name_list[i]);
		i++;
	}

	free(name_list);

	closedir(directory);

	return;
}
// Functions-------------------------------------------------------------------

// Execution-------------------------------------------------------------------
int main()
{
	// get user's home directory path, then append local_apps_suffix
	char * home_dir = getenv("HOME");
	char local_apps[128];
	strcpy(local_apps, home_dir);                // copy home_dir contents to local_apps
	strncat(local_apps, local_apps_suffix, 27);  // append 26 characters of local_apps_suffix to the end of local_apps

	// read directories
	read_dir(system_apps);
	read_dir(local_system_apps);
	read_dir(local_apps);

	return 0;
}
// Execution-------------------------------------------------------------------
