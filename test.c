#define TB_IMPL  // for termbox

#include "termbox.h"

int draw_header()
{
	int y = 0;

	tb_printf(0, y++, TB_GREEN, 0, "hello from termbox");
	tb_printf(0, y++, 0, 0, "width=%d height=%d", tb_width(), tb_height());
	tb_printf(0, y++, 0, 0, "press a key...");
	y++;  // skip a line

	return y;
}

void draw_footer(int y)
{
	y++;  // skip a line
	tb_printf(0, y++, 0, 0, "Press q to quit.");
}

int main(int argc, char * argv[])
{
	int is_first_loop = 1;

	struct tb_event ev;
	// initialize event
	ev.type = 1;
	ev.key  = 0;
	ev.ch   = 'a';

	int y = 0;

	tb_init();

	// Main Loop----------------------------------------------------------------
	while (ev.ch != 'q')
	{
		// update screen
		tb_clear();  // erase screen

		y = draw_header();

		if (!is_first_loop)
			tb_printf(0, y++, 0, 0, "event type=%d key=%d ch=%c", ev.type, ev.key, ev.ch);

		draw_footer(y);

		tb_present();  // draw to screen

		// get input
		tb_poll_event(&ev);

		if (is_first_loop)
			is_first_loop = 0;
	}
	// Main Loop----------------------------------------------------------------

	tb_shutdown();

	return 0;
}
